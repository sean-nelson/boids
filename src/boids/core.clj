(ns boids.core
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [boids.boid :as b]
            [boids.conf :as conf]))

(defn setup []
  ; Set frame rate to 115 frames per second.
  (q/frame-rate conf/framerate)
  ; setup function returns initial state. It contains
  ; circle color and position.
  {:boids (repeatedly conf/boid-count b/create-random-boid)})

(defn update-state [state]
  {:boids (map #(b/update-boid % (:boids state)) (:boids state))})

(defn draw-state [state]
  ; Clear the sketch by filling it with background color.
  (apply q/background conf/background-color)
  (doseq [boid (:boids state)]
    (b/draw-boid boid)))

(q/defsketch boids
  :title "Boids"
  :size conf/frame-size
  ; setup function called only once, during sketch initialization.
  :setup setup
  ; update-state is called on each iteration before draw-state.
  :update update-state
  :draw draw-state
  :features [:keep-on-top]
  ; This sketch uses functional-mode middleware.
  ; Check quil wiki for more info about middlewares and particularly
  ; fun-mode.
  :middleware [m/fun-mode])
