(ns boids.debug
  (:require [boids.conf :as conf]
            [boids.vector :refer [set-magnitude]]
            [quil.core :as q]
            [clojure.core.matrix :as mat]))

(defn in-debug-mode? [& debug-keys]
  (boolean (not-empty (clojure.set/intersection (into #{:all} debug-keys) conf/debug-modes))))

(defn draw-debug-vectors [{position :position :as _boid} center-of-mass average-velocity avoidance-vector target-point]
  (when (and (in-debug-mode? :center-of-mass)
             (not (nil? center-of-mass)))
    (q/stroke 255 0 0)
    (apply q/line (into position (mat/add position center-of-mass))))
  (when (and (in-debug-mode? :average-velocity)
             (not (nil? average-velocity)))
    (q/stroke 0 255 0)
    (apply q/line (into position (mat/add position (set-magnitude 40 average-velocity)))))
  (when (and (in-debug-mode? :avoidance-vector)
             (not (nil? avoidance-vector)))
    (q/stroke 0 0 255)
    (apply q/line (into position (mat/add position (set-magnitude 40 avoidance-vector)))))
  (when (and (in-debug-mode? :target-point)
             (not (nil? target-point)))
    (q/stroke 255 255 0)
    (apply q/line (into position (mat/add position target-point)))))
