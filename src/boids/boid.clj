(ns boids.boid
  (:require [clojure.core.matrix :as mat]
            [boids.conf :as conf]
            [boids.debug :refer [in-debug-mode? draw-debug-vectors]]
            [boids.vector :refer [direction direction-to-turn polar->cart set-magnitude]]
            [quil.core :as q]))

;; TODO: maybe belongs in conf
(def vision-range 150)
;; This is an angle in radians
(def steering-speed 0.05)
;; range at which a boid will start to steer away from another boid
(def too-close 50)

(def vector-weights {:com-vector 1
                     :average-velocity 1
                     :avoidance-vector 1
                     :avoid-walls 10
                     :target-point 5})

;; example boid
(comment {:position [100 100]
          :velocity [1 5]
          :color [255 255 255]
          :target-point {:position [50 150]
                         :lifetime 60}})

;; boid helper functions
(defn random-point []
  "Generates a random coordinate within the bounds of the window"
  (let [[max-x max-y] conf/frame-size]
    [(rand max-x) (rand max-y)]))

(defn create-random-boid []
  (let [max-xvel 3
        max-yvel 3]
    {:position (random-point)
     :velocity [(- (rand (* max-xvel 2)) max-xvel) (- (rand (* max-yvel 2)) max-yvel)]
     :color (nth conf/boid-colors (rand-int (count conf/boid-colors)))}))

(defn draw-boid [{pos :position
                  vel :velocity
                  color :color}]
  (apply q/stroke color)
  (apply q/fill color)
  (q/with-translation pos
    (q/with-rotation [(+ (direction vel) (/ Math/PI 2))]
      (q/triangle -7 20 0 0 7 20))
    (when (in-debug-mode? :vision-range)
      (apply q/fill (conj color 10))
      (q/ellipse 0 0 vision-range vision-range))))

(defn update-boid-position [{vel :velocity pos :position :as boid}]
  (assoc boid :position (mat/add pos vel)))

(defn get-neighbors [{this-position :position :as this-boid} all-boids]
  (filter (fn [{other-position :position :as other-boid}]
            (and (< (mat/distance this-position other-position) vision-range)
                 (not= this-boid other-boid))) all-boids))

(defn steer-towards-direction [boid desired-vector]
  (if (nil? desired-vector)
    boid
    (let [boid-direction (direction (:velocity boid))
          desired-direction (direction desired-vector)
          boid-magnitude (mat/magnitude (:velocity boid))
          dir-difference (- desired-direction boid-direction)
          steering-direction (direction-to-turn (:velocity boid) desired-vector)
          new-direction (+ boid-direction
                           (* (Math/min steering-speed (Math/abs dir-difference))
                              steering-direction))]
      (assoc boid :velocity (polar->cart boid-magnitude new-direction)))))

(defn steer-towards-point [boid point]
  (if (nil? point)
    boid
    (let [direction-to-com  (mat/sub point (:position boid))]
      (steer-towards-direction boid direction-to-com))))

(defn avoid-walls [{[xpos ypos] :position :as boid}]
  (let [min-distance 50
        right-wall (first conf/frame-size)
        bottom-wall (second conf/frame-size)
        desired-direction-vector (mat/add
                                  ;; close to left wall
                                  (if (< xpos min-distance)
                                    [1 0] [0 0])
                                  ;; close to right wall
                                  (if (< (- right-wall xpos) min-distance)
                                    [-1 0] [0 0])
                                  ;; close to top wall
                                  (if (< ypos min-distance)
                                    [0 1] [0 0])
                                  ;; close to bottom wall
                                  (if (< (- bottom-wall ypos) min-distance)
                                    [0 -1] [0 0]))]
    (when-not (= desired-direction-vector [0 0])
      desired-direction-vector)))

(defn generate-target-point [{target-point :target-point :as boid}]
  (if (or (not (nil? target-point))
          ;; each boid should get a target point roughly every ten seconds (at 60fps)
          (> (rand) (/ 1 3000)))
    boid
    (assoc boid :target-point {:position (random-point)
                               ;; hardcode liftime of 180 frames
                               :lifetime 180})))

(defn update-target-point [{target-point :target-point :as boid}]
  (if (nil? target-point)
    boid
    (if (< (:lifetime target-point) 1)
      (assoc boid :target-point nil)
      (assoc boid :target-point {:position (:position target-point)
                                 :lifetime (- (:lifetime target-point) 1)}))))

(defn change-speed [{current-velocity :velocity :as boid} desired-speed]
  (if (nil? desired-speed)
    boid
    (let [speed-change 0.1
          current-speed (mat/magnitude current-velocity)
          current-direction (direction current-velocity)
          speed-diff (- desired-speed current-speed)
          new-speed (+ current-speed (* (min speed-change (Math/abs speed-diff))
                                        (Math/signum speed-diff)))]
      (assoc boid :velocity (polar->cart new-speed current-direction)))))

(defn all-boids-calculations [{this-position :position
                               this-velocity :velocity
                               :as this-boid} all-boids]
  (let [neighbors (get-neighbors this-boid all-boids)
        num-neighbors (count neighbors)
        summation-map (reduce (fn [summation-map {other-position :position
                                                  other-velocity :velocity
                                                  :as other-boid}]
                                (-> (assoc summation-map :position-sum (mat/add other-position (:position-sum summation-map)))
                                    (assoc :velocity-sum (mat/add other-velocity (:velocity-sum summation-map)))
                                    (assoc :speed-sum (+ (mat/magnitude other-velocity) (:speed-sum summation-map)))
                                    (assoc :avoidance-vector
                                           (if (> (mat/distance this-position other-position) too-close)
                                             (:avoidance-vector summation-map)
                                             (mat/add (:avoidance-vector summation-map)
                                                      (mat/scale (mat/sub this-position other-position)
                                                                 (/ 1 (mat/distance this-position other-position))))))))
                              {:position-sum [0 0]
                               :velocity-sum [0 0]
                               :speed-sum 0
                               :avoidance-vector [0 0]}
                              neighbors)]
    {:center-of-mass (when-not (= 0 num-neighbors)
                       (mat/div (mat/add this-position (:position-sum summation-map))
                                (+ 1 num-neighbors)))
     :average-velocity (when-not (= 0 num-neighbors)
                         (mat/div (:velocity-sum summation-map) num-neighbors))
     :average-speed (when-not (= 0 num-neighbors)
                      (/ (:speed-sum summation-map) num-neighbors))
     :avoidance-vector (when-not (= 0 num-neighbors)
                         (:avoidance-vector summation-map))}))

(defn calculate-desired-direction [& vector-maps]
  (let [vectors (filter (fn [{vec :vector}] (not (nil? vec))) vector-maps)
        sum (reduce (fn [vector-sum {vec :vector
                                     mag :magnitude}]
                      (mat/add vector-sum (set-magnitude mag vec))) [0 0] vectors)]
    (if (not= [0 0] sum)
      sum
      nil)))


(defn update-boid [boid all-boids]
  (let [{:keys [center-of-mass
                average-velocity
                average-speed
                avoidance-vector]} (all-boids-calculations boid all-boids)
        com-vector (when-not (nil? center-of-mass)
                     (mat/sub center-of-mass (:position boid)))
        target-vector (when-not (nil? (:target-point boid))
                        (mat/sub (:position (:target-point boid)) (:position boid)))
        desired-direction (calculate-desired-direction {:magnitude (:com-vector vector-weights)
                                                        :vector com-vector}
                                                       {:magnitude (:average-velocity vector-weights)
                                                        :vector average-velocity}
                                                       {:magnitude (:avoidance-vector vector-weights)
                                                        :vector avoidance-vector}
                                                       {:magnitude (:avoid-walls vector-weights)
                                                        :vector (avoid-walls boid)}
                                                       {:magnitude (:target-point vector-weights)
                                                        :vector target-vector})]
    (draw-debug-vectors boid com-vector average-velocity avoidance-vector target-vector)
    (-> boid
        (generate-target-point)
        (update-target-point)
        (change-speed average-speed)
        (steer-towards-direction desired-direction)
        update-boid-position)))
