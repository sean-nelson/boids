(ns boids.conf)


(def background-color [40 42 54])
(def boid-colors [[139 233 253]   ; Cyan
                  [80 250 123]    ; Green
                  [255 184 108]   ; Orange
                  [255 121 198]   ; Pink
                  [189 147 249]   ; Purple
                  [255 85 85]     ; Red
                  [241 250 140]]) ; Yellow

(def boid-size [10 10])
(def boid-count 30)

(def framerate 60)
(def frame-size [1200 780])

;; Accepts a set containing zero or more of the following keywords:
;; :all
;; :vision-range
;; :avoidance-vector
;; :average-velocity
;; :center-of-mass
;; :target-point
(def debug-modes #{})
