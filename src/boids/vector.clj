(ns boids.vector
  (:require [clojure.core.matrix :as mat]))

(defn direction [[x y]]
  "Given a 2D vector `v`, returns an angle in radians representing its angle in polar space"
  (Math/atan2 y x))

(defn direction-to-turn [current desired]
  "Given 2D vectors `current` and `desired`. Returns -1 or 1 representing a steering direction towards the desired angle"
  (let [current-3d (conj current 0)
        desired-3d (conj desired 0)
        turn (-> (mat/cross current-3d desired-3d)
                 last
                 Math/signum)]
    (if (= 0.0 turn)
      1.0
      turn)))

(defn polar->cart [magnitude angle]
  [(* magnitude (Math/cos angle))
   (* magnitude (Math/sin angle))])

(defn set-magnitude [magnitude vector]
  (polar->cart magnitude (direction vector)))
